package com.muddled.engine;

import com.muddled.game.models.*;

public class GameState {
    private GameEngine gameEngine;
    private Player player;

    public GameState(GameEngine gameEngine, Player player) {
        this.gameEngine = gameEngine;
        this.player = player;
        Room startingRoom = new Room("Starting room");
        Room otherRoom = new Room("Other room");
        Room thirdRoom = new Room("Third room");

        startingRoom.addExit(Direction.NORTH, otherRoom);
        startingRoom.addExit(Direction.EAST, thirdRoom);

        otherRoom.addExit(Direction.SOUTH, startingRoom);
        otherRoom.addExit(Direction.SOUTHEAST, thirdRoom);

        thirdRoom.addExit(Direction.NORTHWEST, otherRoom);
        thirdRoom.addExit(Direction.WEST, startingRoom);


        player.setRoom(startingRoom);
    }

    public void movePlayer(Room room) {
        player.setRoom(room);
        gameEngine.playerMoved(room);
    }

    public Player getPlayer() {
        return player;
    }
}
