package com.muddled.engine;

import com.muddled.game.models.Direction;
import com.muddled.game.models.Room;
import com.muddled.game.models.actions.Action;
import com.muddled.game.models.Player;
import com.muddled.game.models.actions.LookAction;
import com.muddled.game.models.actions.MoveAction;
import com.muddled.game.models.commands.Command;
import com.muddled.game.models.commands.CommandParser;

import java.util.Scanner;

public class GameEngine {
    private GameState gameState;
    public GameEngine() {
        gameState = new GameState(this, new Player("Tom"));

    }

    public void startGameLoop() {
        Scanner s = new Scanner(System.in);
        String userInput;
        CommandParser commandParser = new CommandParser(this);
        while(true) {
            // get player input
            System.out.print("> ");
            System.out.flush();
            userInput = s.nextLine();
            Command command = commandParser.parseCommand(userInput);
            if(command == null) {
                System.out.println("Invalid command");
                System.out.flush();
            } else {
                Action action;
                Player player = command.getPlayer();
                switch(command.getName()) {
                    case MOVE:
                        action = new MoveAction(this, Direction.getDirection(command.getCommandText()));
                        action.setOriginator(player);
                        action.perform();
                        break;
                    case LOOK:
                        action = new LookAction(this, null);
                        action.setOriginator(player);
                        action.perform();
                        break;
                }
            }
            // update state
            // tell player of new state
        }
    }

    public GameState getGameState() {
        return gameState;
    }

    public void playerMoved(Room room) {
        // todo: subscribers of this event
        display(room.render());
    }

    public void display(String output) {
        System.out.println(output);
    }
}
