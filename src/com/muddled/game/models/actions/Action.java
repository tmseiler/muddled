package com.muddled.game.models.actions;

import com.muddled.game.models.GameObject;
import com.muddled.game.models.Player;

public interface Action {
    public ActionName getName();

    void setOriginator(Player player);
    Player getOriginator();

    void setTarget(GameObject gameObject);
    GameObject getTarget();

    void perform();
}
