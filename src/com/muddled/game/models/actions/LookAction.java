package com.muddled.game.models.actions;

import com.muddled.engine.GameEngine;
import com.muddled.game.models.GameObject;
import com.muddled.game.models.Player;
import com.muddled.game.models.Renderable;

public class LookAction implements Action {
    private final ActionName name;
    private GameEngine gameEngine;
    private Renderable target;
    private Player player;

    public LookAction(GameEngine gameEngine, Renderable target) {
        this.gameEngine = gameEngine;
        if(target == null) {
            this.target = gameEngine.getGameState().getPlayer().getRoom();
        } else {
            this.target = target;
        }
        this.name = ActionName.LOOK;
    }

    @Override
    public void perform() {
        gameEngine.display(target.render());
    }

    @Override
    public ActionName getName() {
        return name;
    }

    @Override
    public void setOriginator(Player player) {
        this.player = player;
    }

    @Override
    public Player getOriginator() {
        return null;
    }

    @Override
    public void setTarget(GameObject gameObject) {
        target = gameObject;
    }

    @Override
    public GameObject getTarget() {
        return null;
    }
}
