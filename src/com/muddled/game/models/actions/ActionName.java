package com.muddled.game.models.actions;

public enum ActionName {
    MOVE,
    LOOK;
}
