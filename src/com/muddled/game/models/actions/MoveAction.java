package com.muddled.game.models.actions;

import com.muddled.engine.GameEngine;
import com.muddled.engine.GameState;
import com.muddled.game.models.*;

public class MoveAction implements Action {
    private final ActionName name;
    private Direction direction;
    private GameEngine gameEngine;
    private Player player;
    private Player originator;
    private GameObject target;
    private GameState gameState;


    public MoveAction(GameEngine gameEngine, Direction direction) {
        this.gameEngine = gameEngine;
        gameState = gameEngine.getGameState();
        player = gameState.getPlayer();
        this.direction = direction;
        this.name = ActionName.MOVE;
    }

    @Override
    public void perform() {
        Exit exit = player.getRoom().getExit(direction);
        if (exit != null) {
            gameState.movePlayer(exit.getDestination());
        } else {
            System.out.println("There is no exit to the " + direction.getName());
        }
    }

    @Override
    public ActionName getName() {
        return name;
    }

    @Override
    public void setOriginator(Player player) {
        originator = player;
    }

    @Override
    public Player getOriginator() {
        return originator;
    }

    @Override
    public void setTarget(GameObject gameObject) {
        target = gameObject;
    }

    @Override
    public GameObject getTarget() {
        return null;
    }

    @Override
    public String toString() {
        return "MoveAction{" +
                "name=" + name +
                ", direction=" + direction +
                ", gameEngine=" + gameEngine +
                ", player=" + player +
                ", originator=" + originator +
                ", target=" + target +
                '}';
    }
}
