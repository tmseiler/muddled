package com.muddled.game.models;

public interface Renderable {
    String render();
}
