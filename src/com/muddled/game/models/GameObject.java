package com.muddled.game.models;

import com.muddled.game.models.actions.Action;
import com.muddled.game.models.actions.ActionName;

import java.util.List;

public class GameObject implements Renderable {
    public List<ActionName> supportedActionNames;

    public boolean isSupported(ActionName actionName) {
        return supportedActionNames.contains(actionName);
    }

    protected boolean enforceSupport(Action action) {
        if(!isSupported(action.getName())) {
            System.out.println("Action " + action.getName() + "is not supported by " + this);
            return false;
        } else {
            return true;
        }
    }

    public String render() {
        return null;
    }
}
