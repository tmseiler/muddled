package com.muddled.game.models.commands;

import com.muddled.game.models.GameObject;
import com.muddled.game.models.Player;

import java.util.List;

public class Command {
    private CommandName name;
    private String commandText;
    private Player player;
    private List<String> arguments;

    public Command(CommandName name, String commandText, Player player, List<String> arguments) {
        this.name = name;
        this.commandText = commandText;
        this.player = player;
        this.arguments = arguments;
    }
    public Command(CommandName name, String commandText, Player player) {
        this.name = name;
        this.commandText = commandText;
        this.player = player;
        this.arguments = null;
    }

    public CommandName getName() {
        return name;
    }

    public String getCommandText() {
        return commandText;
    }

    public Player getPlayer() {
        return player;
    }
}
