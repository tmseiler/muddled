package com.muddled.game.models.commands;

import java.util.*;

public enum CommandName {
    MOVE("north", "n",
            "northwest", "nw",
            "west", "w",
            "southwest", "sw",
            "south", "s",
            "southeast", "se",
            "east", "e",
            "northeast", "ne"),
    LOOK("look", "l");

    private final static Map<String, CommandName> INSTANCES;

    static {
        Map<String, CommandName> map = new HashMap<String, CommandName>();
        for (CommandName commandName : values()) {
            for (String commandString : commandName.commandStrings) {
                if (map.containsKey(commandString)) {
                    throw new IllegalStateException("CommandName '" + commandString + "' duplicated");
                }
                map.put(commandString, commandName);
            }
        }
        INSTANCES = Collections.unmodifiableMap(map);
    }

    private final List<String> commandStrings;

    CommandName(String... commandStrings) {
        this.commandStrings = Collections.unmodifiableList(Arrays.asList(commandStrings));
    }

    public List<String> getCommandStrings() {
        return commandStrings;
    }

    public String getName() {
        return commandStrings.get(0);
    }

    public static Map<String, CommandName> getInstances() {
        return INSTANCES;
    }

    public static CommandName getCommandName(String commandString) {
        return INSTANCES.get(commandString);
    }
}
