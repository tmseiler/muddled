package com.muddled.game.models.commands;

import com.muddled.engine.GameEngine;
import com.muddled.game.models.Player;

import java.util.StringTokenizer;

import static com.muddled.game.models.commands.CommandName.MOVE;
import static com.muddled.game.models.commands.CommandName.LOOK;


public class CommandParser {

    private GameEngine gameEngine;
    private Player player;

    public CommandParser(GameEngine gameEngine) {
        this.gameEngine = gameEngine;
        player = gameEngine.getGameState().getPlayer(); //todo: multi
    }

    public Command parseCommand(String input) {
        StringTokenizer tokenizer = new StringTokenizer(input);

        if(tokenizer.countTokens() < 1) {
            return new Command(CommandName.LOOK, null, player, null);
        }

        String commandText = tokenizer.nextToken();

        CommandName commandName = CommandName.getCommandName(commandText);
        if(commandName == null)
            return null;

        switch(commandName) {
            case MOVE:
                return new Command(MOVE, commandText, player);
            case LOOK:
                return new Command(LOOK, commandText, player);
        }

        return null;
    }
}
