package com.muddled.game.models;

import java.util.*;

public class Room implements Renderable {
    private final Map<Direction, Exit> exitMap = new EnumMap<Direction, Exit>(Direction.class);
    private String name;

    public Room(String name) {
        this.name = name;
    }

    public Exit addExit(Direction direction, Room room) {
        Exit exit = new Exit(direction, room);
        exitMap.put(direction, exit);
        return exit;
    }

    public Exit getExit(Direction direction) {
        return exitMap.get(direction);
    }

    @Override
    public String toString() {
        return name;
    }

    public List<String> getExitStrings() {
        List<String> exitStrings = new ArrayList<String>();
        for (Direction direction : exitMap.keySet())
            exitStrings.add(direction.getName());
        if(exitStrings.size() < 1) {
            exitStrings.add("NONE!!!");
        }
        return exitStrings;
    }

    public GameObject findByName(String input) {
        return null;
        //todo: should room do this or gamestate?
    }

    public String render() {
        return name + "\nexits:" + this.getExitStrings();
    }
}
