package com.muddled.game.models;

import com.muddled.game.models.actions.ActionName;
import java.util.ArrayList;

public class Exit extends GameObject {
    private Direction direction;
    private Room destination;

    public Exit(Direction direction, Room destination) {
        this.direction = direction;
        this.destination = destination;

        supportedActionNames = new ArrayList<ActionName>();
        supportedActionNames.add(ActionName.MOVE);
        supportedActionNames.add(ActionName.LOOK);
    }

    public Room getDestination() {
        return destination;
    }

    @Override
    public String toString() {
        return direction.getName();
    }
}
