package com.muddled;

import com.muddled.engine.GameEngine;

public class Main {

    public static void main(String[] args) {
        GameEngine engine = new GameEngine();
        engine.startGameLoop();
    }
}
